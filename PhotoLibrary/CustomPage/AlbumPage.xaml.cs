﻿using PhotoLibrary.ObjectsJson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PhotoLibrary
{
    /// <summary>
    /// Логика взаимодействия для AlbumPage.xaml
    /// </summary>
    public partial class AlbumPage : Page
    {
        private List<AlbumObject> arr;
        private Window wnd;
        public AlbumPage()
        {
            InitializeComponent();
            this.Loaded += PageLoaded;
        }
        private void PageLoaded(object sender, RoutedEventArgs e)
        {
            AddAlbumToScreen();
        }

        private void AddAlbumToScreen()
        {
            try
            {
                WorkArea.Children.RemoveRange(1, WorkArea.Children.Count - 1);
            }
            catch (Exception ex) { }
            arr = StructureSingleton.getInstance().getAlbumByUsers();

            foreach (var item in arr)
            {
                UserImageControl img = new UserImageControl(item.PreviewPath, item.Id);
                img.MouseLeftButtonDown += ShowOnClick;
                WorkArea.Children.Add(img);

            }
        }

        private void ShowOnClick(object sender, MouseButtonEventArgs e)
        {
            wnd = Window.GetWindow(this);
            (wnd as MainWindow).Main.Content = new AlbumDetailPage(((UserImageControl)sender).Id);

        }

        private void AddAlbum_Click(object sender, RoutedEventArgs e)
        {
            CreateAlbum create = new CreateAlbum();
            create.Owner = Window.GetWindow(this);
            AnimationUtil.ApplyShadowEffect(Window.GetWindow(this) as MainWindow);
            if (create.ShowDialog() == true)
            {
                StructureSingleton.getInstance().addAlbum(new AlbumObject(create.Name, create.Path, StructureSingleton.getInstance().CurrentUser.Id));
                Window wnd = Window.GetWindow(this);

                AddAlbumToScreen();
                (wnd as MainWindow).AlbumCount.Text = arr.Count.ToString();
            }
            AnimationUtil.ApplyShadowEffect(Window.GetWindow(this) as MainWindow);
        }


        public void OnChangeUser()
        {
            AddAlbumToScreen();
        }
    }


}
