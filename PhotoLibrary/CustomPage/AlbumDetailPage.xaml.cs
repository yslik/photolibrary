﻿using PhotoLibrary.CustomWindows;
using PhotoLibrary.ObjectsJson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PhotoLibrary
{
    /// <summary>
    /// Логика взаимодействия для AlbumDetailPage.xaml
    /// </summary>
    public partial class AlbumDetailPage : Page
    {
        private List<PhotoObject> arr;
        private int AlbumId { get; set; }
        public AlbumDetailPage(int id)
        {
            InitializeComponent();
            AlbumId = id;
            this.Loaded += PageLoaded;
        }
        private void PageLoaded(object sender, RoutedEventArgs e)
        {
            AddPhotoToScreen();
        }
        private void AddPhotoToScreen()
        {
            try
            {
                WorkArea.Children.RemoveRange(1, WorkArea.Children.Count - 1);
            }
            catch (Exception ex) { }
            arr = StructureSingleton.getInstance().getPhotosByAlbumAndUsers(AlbumId);

            foreach (var item in arr)
            {
                UserImageControl img = new UserImageControl(item.Path);
                img.MouseLeftButtonDown += ShowOnClick;
                WorkArea.Children.Add(img);

            }
        }

        private void ShowOnClick(object sender, RoutedEventArgs e)
        {
            PhotoDetail userWindow = new PhotoDetail();
            userWindow.SetContent((sender as UserImageControl).Path);
            userWindow.ShowDialog();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var wnd = Window.GetWindow(this);
            (wnd as MainWindow).Main.Content = (wnd as MainWindow).albumPage;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            StructureSingleton.getInstance().deleteAlbum(AlbumId);

            var wnd = Window.GetWindow(this);
            (wnd as MainWindow).Main.Content = (wnd as MainWindow).albumPage;

            MessageBoxCustom msg = new MessageBoxCustom("Album Id = " + AlbumId + " deleted" );

        }
    }
}
