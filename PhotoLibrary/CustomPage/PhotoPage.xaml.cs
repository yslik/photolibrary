﻿using Microsoft.Win32;
using PhotoLibrary.CustomWindows;
using PhotoLibrary.ObjectsJson;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PhotoLibrary
{
    /// <summary>
    /// Логика взаимодействия для PhotoPage.xaml
    /// </summary>
    public partial class PhotoPage : Page
    {
        List<PhotoObject> arr;
        PhotoDetail userWindow;

        public PhotoPage()
        {
            InitializeComponent();
            this.Loaded += PageLoaded;
        }

        private void PageLoaded(object sender, RoutedEventArgs e)
        {
            AddPhotoToScreen();
        }

        private void AddPhotoToScreen()
        {
            try
            {
                WorkArea.Children.RemoveRange(1, WorkArea.Children.Count - 1);
            }
            catch (Exception ex) { }
            arr = StructureSingleton.getInstance().getPhotosByUsers();

            foreach (var item in arr)
            {
                UserImageControl img = new UserImageControl(item.Path);
                img.MouseLeftButtonDown += ShowOnClick;
                WorkArea.Children.Add(img);

            }

        }

        private void ShowOnClick(object sender, RoutedEventArgs e)
        {
            userWindow = new PhotoDetail();
            userWindow.SetContent((sender as UserImageControl).Path);
            userWindow.ShowDialog();
        }             

        private void AddPhoto_Click(object sender, RoutedEventArgs e)
        {
            AddPhotoWindow dialog = new AddPhotoWindow();
            dialog.Owner = Window.GetWindow(this);
            AnimationUtil.ApplyShadowEffect(Window.GetWindow(this) as MainWindow);
            if (dialog.ShowDialog() == true)
            {
                StructureSingleton.getInstance().addPhoto(new PhotoObject(dialog.Path, dialog.AlbumId, StructureSingleton.getInstance().CurrentUser.Id));
                AddPhotoToScreen();
                Window wnd = Window.GetWindow(this);
                (wnd as MainWindow).PhotoCount.Text = arr.Count.ToString();
            }
            AnimationUtil.ApplyShadowEffect(Window.GetWindow(this) as MainWindow);


        }

        public void onChangeUser()
        {
            AddPhotoToScreen();
        }
    }
}
