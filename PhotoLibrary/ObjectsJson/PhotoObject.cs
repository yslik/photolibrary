﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PhotoLibrary.ObjectsJson
{
    [DataContract]
    class PhotoObject
    {
        public static int currentId = 0;

      
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Path { get; set; }
        [DataMember]
        public int AlbumId { get; set; }
        [DataMember]
        public int UserId { get; set; }



        public PhotoObject( string path, int albumid, int userId)
        {

            
            Id = ++Properties.Settings.Default.LastPhotoId;
            Properties.Settings.Default.LastPhotoId = Id;
            Properties.Settings.Default.Save();
            Path = path;
            AlbumId = albumid;
            UserId = userId;

        }

    }
}
