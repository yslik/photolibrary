﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PhotoLibrary.ObjectsJson
{
    [DataContract]
    class UserObject

    {
        public static int currentId = 0;

        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Info { get; set; }
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Path { get; set; }


        public UserObject(string name, string info, string photoPath)
        {
            Name = name;
            Info = info;
            Id = ++Properties.Settings.Default.LastUserId;
            Properties.Settings.Default.LastUserId = Id;
            Properties.Settings.Default.Save();
            Path = photoPath;
        }

    }
}
