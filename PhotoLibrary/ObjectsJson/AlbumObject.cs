﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PhotoLibrary.ObjectsJson
{
    [DataContract]
    class AlbumObject
    {
        
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int UserId { get; set; }
        [DataMember]
        public string PreviewPath { get; set; }

        public AlbumObject(string name, string path, int userId)
        {

            Name = name;
            Id = ++Properties.Settings.Default.LastAlbumId;
            Properties.Settings.Default.LastAlbumId = Id;
            Properties.Settings.Default.Save();
            UserId = userId;
            PreviewPath = path;
        }
    }
}
