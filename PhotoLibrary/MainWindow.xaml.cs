﻿using Microsoft.Win32;
using PhotoLibrary.CustomWindows;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace PhotoLibrary
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        PhotoPage photoPage;
        public AlbumPage albumPage { get; set; }

        OleDbConnection con;
        DataTable dt;

        public MainWindow()
        {
            InitializeComponent();
            this.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            photoPage = new PhotoPage();
            albumPage = new AlbumPage();
            this.Loaded += MainWindow_Loaded;
            this.Closing += MainWindow_Closing;
     
            
        }

        private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Properties.Settings.Default.LastUsingId = StructureSingleton.getInstance().CurrentUser.Id;
            Properties.Settings.Default.Save();
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {           
           
            Main.Content = photoPage;
            ControlTemplate style = this.FindResource("btnAllStyleActive") as ControlTemplate;
            AllPhotos.Template = style;
            if (StructureSingleton.getInstance().CurrentUser.Path.Contains("noavatar.png"))
            {
                this.UserImg.Source = new BitmapImage(new Uri("pack://application:,,,/PhotoLibrary;component/" + StructureSingleton.getInstance().CurrentUser.Path));
            }
            else this.UserImg.Source = new BitmapImage(new Uri(StructureSingleton.getInstance().CurrentUser.Path));
            setNewUser();
        }

        private void setNewUser()
        {
            this.UserInfo.Text = StructureSingleton.getInstance().CurrentUser.Info;
            this.UserName.Text = StructureSingleton.getInstance().CurrentUser.Name;
            try
            {
                this.UserImg.Source = new BitmapImage(new Uri(StructureSingleton.getInstance().CurrentUser.Path));
            }
            catch (Exception ex)
            {
                
            }
            this.PhotoCount.Text = StructureSingleton.getInstance().countPhotos().ToString();
            this.AlbumCount.Text = StructureSingleton.getInstance().countAlbums().ToString();
            photoPage.onChangeUser();
            albumPage.OnChangeUser();

        }
        


        private void Minimize_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        private void Close_Click(object sender, RoutedEventArgs e)
        {
            Duration interval = new Duration(TimeSpan.FromMilliseconds(500));
            DoubleAnimation animation = new DoubleAnimation(0, interval);
            animation.Completed += Animation_Completed;
            mainWindow.BeginAnimation(OpacityProperty, animation);


        }

        private void Animation_Completed(object sender, EventArgs e)
        {
            Close();
        }

        private void SizeMode_Click(object sender, RoutedEventArgs e)
        {
            if (this.WindowState == WindowState.Maximized)
            {
                SizeBtnImg.Source = (BitmapImage)this.TryFindResource("imgFullscreen");
                this.WindowState = WindowState.Normal;

            }
            else if (this.WindowState == WindowState.Normal)
            {
                SizeBtnImg.Source = (BitmapImage)this.TryFindResource("imgFullscreen_exit");
                this.WindowState = WindowState.Maximized;

            }
        }
                    
        private void AllPhotos_Click(object sender, RoutedEventArgs e)
        {
            ControlTemplate style = this.FindResource("btnAllStyleActive") as ControlTemplate;
            AllPhotos.Template = style;
            ControlTemplate style1 = this.FindResource("btnAllStyle") as ControlTemplate;
            AllAbums.Template = style1;
            Main.Content = photoPage;

        }

        private void AllAbums_Click(object sender, RoutedEventArgs e)
        {
            ControlTemplate style = this.FindResource("btnAllStyleActive") as ControlTemplate;
            AllAbums.Template = style;
            ControlTemplate style1 = this.FindResource("btnAllStyle") as ControlTemplate;
            AllPhotos.Template = style1;
            Main.Content = albumPage;

        }

        private void CorrectUser_Click(object sender, RoutedEventArgs e)
        {
            if (CorrectUser.Content.Equals("Correct User"))
            {
                UserName.IsEnabled = true;
                UserInfo.IsEnabled = true;
                CorrectUser.Content = "Save changes";
            }
            else
            {
                UserName.IsEnabled = false;
                UserInfo.IsEnabled = false;
                CorrectUser.Content = "Correct User";
                StructureSingleton.getInstance().correctUser(UserName.Text, UserInfo.Text);
            }
        }

        private void ChangeUser_Click(object sender, RoutedEventArgs e)
        {
            UserWindow userWindow = new UserWindow();
            userWindow.Owner = this;
            AnimationUtil.ApplyShadowEffect(this);
            if (userWindow.ShowDialog()==true)
            {
                setNewUser();
            }
            AnimationUtil.ApplyShadowEffect(this);

        }
        
        private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            try {
                this.DragMove();
            }
            catch (Exception ex)
            {
                
            }
          
        }

        private void AddUser_Click(object sender, RoutedEventArgs e)
        {
            AddUser user = new AddUser();
            user.Owner = this;
            AnimationUtil.ApplyShadowEffect(this);
            user.ShowDialog();
            AnimationUtil.ApplyShadowEffect(this);
        }
    }
}
