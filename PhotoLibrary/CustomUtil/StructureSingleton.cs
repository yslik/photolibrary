﻿using PhotoLibrary.ObjectsJson;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;

namespace PhotoLibrary
{
    class StructureSingleton
    {

        private static StructureSingleton instance;
        public static StructureSingleton getInstance()
        {
            if (instance == null)
                instance = new StructureSingleton();
            return instance;
        }
        private List<PhotoObject> photos;
        private List<AlbumObject> albums;
        private List<UserObject> users;
        public UserObject CurrentUser { get; set; }

        //TODO название файлов в конфигурацию
        private StructureSingleton()
        {
            albums = new List<AlbumObject>();
            photos = new List<PhotoObject>();
            users = new List<UserObject>();

            GetUsersFromFile();

            CurrentUser = users.Find(x=>x.Id==Properties.Settings.Default.LastUsingId);

            if (CurrentUser==null)
            {
                UserObject obj = new UserObject("NoName", "No info", "Images/noavatar.png");
                users.Add(obj);
                CurrentUser = obj;
                addUsersToFile();
            }
            

        }

        public List<PhotoObject> getPhotos()
        {
            getPhotosFromFile();
            return photos;
        }

        public List<AlbumObject> getAlbums()
        {
            getAlbumsFromFile();
            return albums;
        }
        public List<UserObject> getUsers()
        {
            GetUsersFromFile();
            return users;
        }



        public int countPhotos()
        {
            return getPhotosByUsers().Count;
        }
        public int countAlbums()
        {
            return getAlbumByUsers().Count;
        }


        //PHOTO/////////
        public void addPhoto(PhotoObject obj)
        {
            photos.Add(obj);
            addPhotosToFile();
        }

        private void getPhotosFromFile()
        {
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(List<PhotoObject>));
            using (FileStream fs = new FileStream(Properties.Settings.Default.PhotoFilePath, FileMode.OpenOrCreate))
            {
                try
                {
                    photos = (List<PhotoObject>)serializer.ReadObject(fs);
                }
                catch (Exception ex)
                {

                }

            }
        }
        private void addPhotosToFile()
        {
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(List<PhotoObject>));
            using (FileStream fs = new FileStream(Properties.Settings.Default.PhotoFilePath, FileMode.Truncate))
            {
                serializer.WriteObject(fs, photos);
            }
        }

        public List<PhotoObject> getPhotosByAlbum(int albumId)
        {
            List<PhotoObject> result = new List<PhotoObject>();
            getPhotosFromFile();
            foreach (var item in photos)
            {
                if (item.AlbumId == albumId)
                {
                    result.Add(item);
                }
            }
            return result;
        }

        public List<PhotoObject> getPhotosByAlbumAndUsers(int albumId)
        {
            getPhotosFromFile();
            List<PhotoObject> result = new List<PhotoObject>();
            foreach (var item in photos)
            {
                if (item.AlbumId == albumId && item.UserId == CurrentUser.Id)
                {
                    result.Add(item);
                }
            }
            return result;
        }
        public List<PhotoObject> getPhotosByUsers()
        {
            getPhotosFromFile();
            List<PhotoObject> result = new List<PhotoObject>();
            foreach (var item in photos)
            {
                if (item.UserId == CurrentUser.Id)
                {
                    result.Add(item);
                }
            }
            return result;
        }
        public bool deletePhoto(int id)
        {
            getPhotosFromFile();
            try
            {
                photos.Remove(photos.Find(x => x.Id == id));
                addPhotosToFile();
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        //ALBUM //////
        public void addAlbum(AlbumObject obj)
        {
            getAlbumsFromFile();
            albums.Add(obj);
            addAlbumsToFile();
        }

        private void getAlbumsFromFile()
        {
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(List<AlbumObject>));
            using (FileStream fs = new FileStream(Properties.Settings.Default.AlbumsFilePath, FileMode.OpenOrCreate))
            {
                try
                {
                    albums = (List<AlbumObject>)serializer.ReadObject(fs);
                }
                catch (Exception ex)
                {

                }
            }
        }
        private void addAlbumsToFile()
        {
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(List<AlbumObject>));
            using (FileStream fs = new FileStream(Properties.Settings.Default.AlbumsFilePath, FileMode.Truncate))
            {
                serializer.WriteObject(fs, albums);
            }
        }
        public List<AlbumObject> getAlbumByUsers()
        {
            getAlbumsFromFile();
            List<AlbumObject> result = new List<AlbumObject>();
            foreach (var item in albums)
            {
                if (item.UserId == CurrentUser.Id)
                {
                    result.Add(item);
                }
            }
            return result;
        }

        public bool deleteAlbum(int id)
        {
            getAlbumsFromFile();
            try
            {
                albums.Remove(albums.Find(x => x.Id == id));
                foreach (var item in photos)
                {
                    if (item.AlbumId == id) item.AlbumId = -1;
                }
                addAlbumsToFile();
                addPhotosToFile();
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        //USER////////////
        public void AddUser(UserObject obj)
        {
            GetUsersFromFile();
            users.Add(obj);
            addUsersToFile();
        }

        private void GetUsersFromFile()
        {
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(List<UserObject>));
            using (FileStream fs = new FileStream(Properties.Settings.Default.UsersPath, FileMode.OpenOrCreate))
            {
                users = (List<UserObject>)serializer.ReadObject(fs);
            }
        }
        private void addUsersToFile()
        {
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(List<UserObject>));
            using (FileStream fs = new FileStream(Properties.Settings.Default.UsersPath, FileMode.Truncate))
            {
                serializer.WriteObject(fs, users);
            }
        }
        public void correctUser(string name, string info)
        {
            GetUsersFromFile();
            users.ForEach(x =>
            {
                if (x.Id == CurrentUser.Id)
                {
                    x.Info = info;
                    x.Name = name;
                }
            }
            );

            addUsersToFile();

        }
    }
}
