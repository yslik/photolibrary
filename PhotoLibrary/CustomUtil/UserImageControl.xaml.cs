using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace PhotoLibrary
{
    public partial class UserImageControl : UserControl
    {
        public string Path { get; set; } 
        public int Id { get; set; }
       
       
        public UserImageControl()
        {
            InitializeComponent();
        }
        public UserImageControl(string path)
        {
            InitializeComponent();
            Path = path; 
            this.Loaded += ItemOnLoaded;        
        }
        public UserImageControl(string path, int id)
        {
            InitializeComponent();
            Path = path;
            Id = id;
            this.Loaded += ItemOnLoaded;
        }
        private void ItemOnLoaded(object sender, RoutedEventArgs e)
        {
            Image img = new Image { Source = new BitmapImage(new Uri(Path)) };
            img.Width = 200;
            img.Height = 200;
            img.Stretch = System.Windows.Media.Stretch.Fill;
           
            if(this.Content==null) this.AddChild(img);
            

        }



    }
}