﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoLibrary
{
    static class AnimationUtil
    {
         public static void ApplyShadowEffect(MainWindow wnd)
            {
                if (wnd.Effect == null)
                {
                    System.Windows.Media.Effects.BlurEffect objBlur = new System.Windows.Media.Effects.BlurEffect();
                    objBlur.Radius = 20;
                    wnd.Effect = objBlur;

                }
                else wnd.Effect = null;

            }


    }
   

}
