﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PhotoLibrary.CustomWindows
{
    /// <summary>
    /// Логика взаимодействия для AddUser.xaml
    /// </summary>
    public partial class AddUser : Window

    {
        public string Path { get; set; }
        public AddUser()
        {
            InitializeComponent();
            this.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterOwner;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (Path != null)
            {
                StructureSingleton.getInstance().AddUser(new ObjectsJson.UserObject(UserName.Text, UserInfo.Text, Path));
                this.DialogResult = true;
            }
            else Error.Visibility = Visibility.Visible;
        }

        private void File_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Image (*.jpg)|*.jpg;*.jpeg;*.png;";
            if (dialog.ShowDialog() == true)
            {
                this.Path = dialog.FileName;
                this.File.Content = dialog.FileName;
            }
        }
    }
}
