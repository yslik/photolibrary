﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PhotoLibrary
{
    /// <summary>
    /// Логика взаимодействия для Window1.xaml
    /// </summary>
    public partial class PhotoDetail : Window
    {
      
        public PhotoDetail()
        {
            InitializeComponent();
            this.Width = System.Windows.SystemParameters.PrimaryScreenWidth;
            this.Height = System.Windows.SystemParameters.PrimaryScreenHeight;
            this.WindowStartupLocation = System.Windows.WindowStartupLocation.Manual;
            this.Left = 0;
            this.Top = 0;

        }
        public void SetContent(string _path)
        {
            ImageWrapper.Source = new BitmapImage(new Uri(_path));
            ImageWrapper.MaxWidth = System.Windows.SystemParameters.PrimaryScreenWidth - 100;
            ImageWrapper.MaxHeight = System.Windows.SystemParameters.PrimaryScreenHeight - 50;
           
        }

        private void MainWindow_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.Close();
        }

       
    }
}
