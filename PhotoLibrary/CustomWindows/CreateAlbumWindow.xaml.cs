﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PhotoLibrary
{
    /// <summary>
    /// Логика взаимодействия для CreateAlbum.xaml
    /// </summary>
    public partial class CreateAlbum : Window
    {
        public String Name { get; set; }
        public String Path { get; set; }
        public CreateAlbum()
        {
            InitializeComponent();
            this.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterOwner;
        }

        private void Button_Click_Ok(object sender, RoutedEventArgs e)
        {
            this.Name = AlbumName.Text;
            if (this.Path != null && this.Name != null)
            {
                this.DialogResult = true;               
            }
            else this.Error.Visibility = Visibility.Visible;
        }

        private void File_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Image |*.jpg;*.jpeg;*.png;";
            if (dialog.ShowDialog() == true)
            {
                this.Path = dialog.FileName;
                this.File.Content = dialog.FileName;
            }
        }
    }
}
