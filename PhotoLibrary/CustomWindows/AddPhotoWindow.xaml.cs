﻿using Microsoft.Win32;
using PhotoLibrary.ObjectsJson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PhotoLibrary.CustomWindows
{
    /// <summary>
    /// Логика взаимодействия для AddPhotoWindow.xaml
    /// </summary>
    public partial class AddPhotoWindow : Window
    {
        private List<AlbumObject> list;
        public string Path { get; set; }
        public int AlbumId { get; set; }

        public AddPhotoWindow()
        {
            InitializeComponent();
            this.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterOwner;
            list = StructureSingleton.getInstance().getAlbumByUsers();
            AlbumId = -1;
            if (list.Count == 0)
            {
                this.albumList.IsEnabled = false;
                this.Attention.Visibility = Visibility.Visible;
            }
            else
            {
                list.ForEach(x =>
                {
                    this.albumList.Items.Add(x.Name);
                }
                );
            }


        }

        private void File_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Image |*.jpg;*.jpeg;*.png;";
            if (dialog.ShowDialog() == true)
            {
                this.Path = dialog.FileName;
                this.File.Content = dialog.FileName;

            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (list.Count > 0)
            {
                AlbumId = list[this.albumList.SelectedIndex].Id;
            }
            
              
           if (this.Path != null && this.AlbumId != -1)
            {
                this.DialogResult = true;
            }

            else this.Error.Visibility = Visibility.Visible;
        }
    }
}
