﻿using PhotoLibrary.ObjectsJson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PhotoLibrary
{
    /// <summary>
    /// Логика взаимодействия для UserWindow.xaml
    /// </summary>
    
    public partial class UserWindow : Window
    {
        private List<UserObject> list;
        public int CurrentUserId{get; set;}
        public UserWindow()
        {
            InitializeComponent();
            this.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterOwner;
            list  = StructureSingleton.getInstance().getUsers();
            if (list.Count == 0)
            {
                this.userList.ToolTip = "List is Empty";
                this.userList.IsEnabled = false;
                this.Attention.Visibility = Visibility.Visible;
            }
            else
            {
                list.ForEach(x =>
                {
                    this.userList.Items.Add(x.Name);
                }
                );
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            StructureSingleton.getInstance().CurrentUser = list[userList.SelectedIndex];
        }

    
    }
}
